<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    $id = $_GET["id"];
    include "conexao.php";
    $sql = "SELECT funcionario.*, cargo.descricao_cargo 
    FROM funcionario 
    INNER JOIN cargo ON funcionario.id_cargo = cargo.id WHERE funcionario.id=$id";

    $resultado = $conn->query($sql);
    while($row = $resultado->fetch_assoc()) {
       ?>
       <form action="editando_funcionario.php" method="post">
            <input type="hidden" name="id" value=" <?php echo $id; ?> "/>
            <div>
                <label> Nome:</label>
                <input type="text" name="nome" value="<?php echo $row["nome"]; ?>"/>
            </div>
            <div>
                <label> Sobrenome:</label>
                <input type="text" name="sobrenome" value="<?php echo $row["sobrenome"]; ?>"/>
            </div>
            <div>
                <label> Cargo:</label>
                <select name="id_cargo">
                    <?php
                        $sql = "SELECT * FROM cargo";

                        $resultado = $conn->query($sql);
                        while($row_cargo = $resultado->fetch_assoc()) {

                            if ($row["id_cargo"] == $row_cargo["id"]){
                                echo "<option selected value=" . $row_cargo["id"] . ">" . $row_cargo["descricao_cargo"] . "</option>";
                            }
                            else{
                                echo "<option value=" . $row_cargo["id"] . ">" . $row_cargo["descricao_cargo"] . "</option>";
                            }

                        }
                    ?>
                </select>
            </div>
            <div>
                <label> Data de Nascimento:</label>
                <input type="date" name="data_de_nascimento" value="<?php echo $row["data_de_nascimento"]; ?>"/>
            </div>
            <div>
                <label> Data de Admissao:</label>
                <input type="date" name="data_de_admissao" value="<?php echo $row["data_de_admissao"]; ?>"/>
            </div>
            <div>
                <label> Salario:</label>
                <input type="text" name="salario" value="<?php echo $row["salario"]; ?>"/>
            </div>
            <div class="button">
                <button type="submit">Enviar sua mensagem</button>
            </div>
        </form>
       <?php
    }

    ?>
    
</body>
</html>