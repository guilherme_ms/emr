<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>
    <form action="inserindo_funcionario.php" method="post">
    <div>
        <label> Nome:</label>
        <input type="text" name="nome" />
    </div>
    <div>
        <label> Sobrenome:</label>
        <input type="text" name="sobrenome" />
    </div>
    <div>
        <label> Cargo:</label>
        <select name="id_cargo">
            <?php
                include "conexao.php";
                $sql = "SELECT * FROM cargo";

                $resultado = $conn->query($sql);
                while($row = $resultado->fetch_assoc()) {
                    echo "<option value=" . $row["id"] . ">" . $row["descricao_cargo"] . "</option>";
                }
            ?>
        </select>
    </div>
    <div>
        <label> Data de Nascimento:</label>
        <input type="date" name="data_de_nascimento" />
    </div>
    <div>
        <label> Data de Admissao:</label>
        <input type="date" name="data_de_admissao" />
    </div>
    <div>
        <label> Salario:</label>
        <input type="text" name="salario" />
    </div>
   
    <div class="button">
        <button type="submit">Enviar sua mensagem</button>
    </div>
</form>
</h1>
</body>
</html>