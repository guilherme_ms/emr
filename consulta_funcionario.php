<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php    
 
include "conexao.php";
$sql = "SELECT funcionario.*, cargo.descricao_cargo 
FROM funcionario 
INNER JOIN cargo ON funcionario.id_cargo = cargo.id";

$resultado = $conn->query($sql);
while($row = $resultado->fetch_assoc()) {
    echo "id: " . $row["id"];
    echo " <br> - Nome: " . $row["nome"];
    echo " <br> - Sobrenome: " . $row["sobrenome"];
    echo " <br> - Cargo: " . $row["descricao_cargo"];
    echo " <br> - Data de Nascimento: " . $row["data_de_nascimento"];
    echo " <br> - Data de Admissao: " . $row["data_de_admissao"];
    echo " <br> - Salario: " . $row["salario"];

    echo "<br> <a href='editar_funcionario.php?id=" . $row["id"] . " '>Editar</a> ";
    echo "<a href='deletar_funcionario.php?id=" . $row["id"] . " '>Deletar</a> <br> <br>";
}
?>
 
</body>
</html>